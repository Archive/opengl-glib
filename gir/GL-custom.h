/**
 * glColor3bv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glColor3dv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glColor3fv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glColor3bv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glColor3dv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glColor3fv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glColor3iv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glColor3sv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glColor3ubv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glColor3uiv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glColor3usv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glColor4bv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glColor4dv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glColor4fv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glColor4iv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glColor4sv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glColor4ubv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glColor4uiv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glColor4usv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glGetMapdv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glGetMapfv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glGetMapiv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glGetPixelMapfv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glGetPixelMapuiv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glGetPixelMapusv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glNormal3bv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glNormal3dv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glNormal3fv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glNormal3iv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glNormal3sv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glPixelMapfv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glPixelMapuiv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glPixelMapusv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glRasterPos2dv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glRasterPos2fv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glRasterPos2iv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glRasterPos2sv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glRasterPos3dv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glRasterPos3fv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glRasterPos3iv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glRasterPos3sv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glRasterPos4dv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glRasterPos4fv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glRasterPos4iv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glRasterPos4sv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glRectdv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glRectfv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glRectiv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glRectsv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glTexCoord1dv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glTexCoord1fv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glTexCoord1iv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glTexCoord1sv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glTexCoord2dv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glTexCoord2fv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glTexCoord2iv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glTexCoord2sv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glTexCoord3dv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glTexCoord3fv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glTexCoord3iv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glTexCoord3sv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glTexCoord4dv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glTexCoord4fv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glTexCoord4iv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glTexCoord4sv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glVertex2dv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glVertex2fv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glVertex2iv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glVertex2sv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glVertex3dv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glVertex3fv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glVertex3iv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glVertex3sv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glVertex4dv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glVertex4fv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glVertex4iv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glVertex4sv:
 * @v: (array zero-terminated=1) (transfer none):
 */
/**
 * glEdgeFlagv:
 * @flag: (array zero-terminated=1) (transfer none):
 */
/**
 * glEvalCoord1dv:
 * @u: (array zero-terminated=1) (transfer none):
 */
/**
 * glEvalCoord1fv:
 * @u: (array zero-terminated=1) (transfer none):
 */
/**
 * glEvalCoord2dv:
 * @u: (array zero-terminated=1) (transfer none):
 */
/**
 * glEvalCoord2fv:
 * @u: (array zero-terminated=1) (transfer none):
 */
/**
 * glFogfv:
 * @params: (array zero-terminated=1) (transfer none):
 */
/**
 * glFogiv:
 * @params: (array zero-terminated=1) (transfer none):
 */
/**
 * glGetBooleanv:
 * @params: (array zero-terminated=1) (transfer none):
 */
/**
 * glGetDoublev:
 * @params: (array zero-terminated=1) (transfer none):
 */
/**
 * glGetFloatv:
 * @params: (array zero-terminated=1) (transfer none):
 */
/**
 * glGetIntegerv:
 * @params: (array zero-terminated=1) (transfer none):
 */
/**
 * glGetLightfv:
 * @params: (array zero-terminated=1) (transfer none):
 */
/**
 * glGetLightiv:
 * @params: (array zero-terminated=1) (transfer none):
 */
/**
 * glGetMaterialfv:
 * @params: (array zero-terminated=1) (transfer none):
 */
/**
 * glGetMaterialiv:
 * @params: (array zero-terminated=1) (transfer none):
 */
/**
 * glGetPixelMapfv:
 * @values: (array zero-terminated=1) (transfer none):
 */
/**
 * glGetPixelMapuiv:
 * @values: (array zero-terminated=1) (transfer none):
 */
/**
 * glGetPixelMapusv:
 * @values: (array zero-terminated=1) (transfer none):
 */
/**
 * glGetPointerv:
 * @params: (array zero-terminated=1) (transfer none):
 */
/**
 * glGetTexEnvfv:
 * @params: (array zero-terminated=1) (transfer none):
 */
/**
 * glGetTexEnviv:
 * @params: (array zero-terminated=1) (transfer none):
 */
/**
 * glGetTexGendv:
 * @params: (array zero-terminated=1) (transfer none):
 */
/**
 * glGetTexGenfv:
 * @params: (array zero-terminated=1) (transfer none):
 */
/**
 * glGetTexGeniv:
 * @params: (array zero-terminated=1) (transfer none):
 */
/**
 * glGetTexLevelParameterfv:
 * @params: (array zero-terminated=1) (transfer none):
 */
/**
 * glGetTexLevelParameteriv:
 * @params: (array zero-terminated=1) (transfer none):
 */
/**
 * glGetTexParameterfv:
 * @params: (array zero-terminated=1) (transfer none):
 */
/**
 * glGetTexParameteriv:
 * @params: (array zero-terminated=1) (transfer none):
 */
/**
 * glIndexdv:
 * @c: (array zero-terminated=1) (transfer none):
 */
/**
 * glIndexfv:
 * @c: (array zero-terminated=1) (transfer none):
 */
/**
 * glIndexiv:
 * @c: (array zero-terminated=1) (transfer none):
 */
/**
 * glIndexsv:
 * @c: (array zero-terminated=1) (transfer none):
 */
/**
 * glIndexubv:
 * @c: (array zero-terminated=1) (transfer none):
 */
/**
 * glLightModelfv:
 * @params: (array zero-terminated=1) (transfer none):
 */
/**
 * glLightModeliv:
 * @params: (array zero-terminated=1) (transfer none):
 */
/**
 * glLightfv:
 * @params: (array zero-terminated=1) (transfer none):
 */
/**
 * glLightiv:
 * @params: (array zero-terminated=1) (transfer none):
 */
/**
 * glMaterialfv:
 * @params: (array zero-terminated=1) (transfer none):
 */
/**
 * glMaterialiv:
 * @params: (array zero-terminated=1) (transfer none):
 */
/**
 * glPixelMapfv:
 * @values: (array zero-terminated=1) (transfer none):
 */
/**
 * glPixelMapuiv:
 * @values: (array zero-terminated=1) (transfer none):
 */
/**
 * glPixelMapusv:
 * @values: (array zero-terminated=1) (transfer none):
 */
/**
 * glRectdv:
 * @v1, const GLdouble *v2: (array zero-terminated=1) (transfer none):
 */
/**
 * glRectfv:
 * @v1, const GLfloat *v2: (array zero-terminated=1) (transfer none):
 */
/**
 * glRectiv:
 * @v1, const GLint *v2: (array zero-terminated=1) (transfer none):
 */
/**
 * glRectsv:
 * @v1, const GLshort *v2: (array zero-terminated=1) (transfer none):
 */
/**
 * glTexEnvfv:
 * @params: (array zero-terminated=1) (transfer none):
 */
/**
 * glTexEnviv:
 * @params: (array zero-terminated=1) (transfer none):
 */
/**
 * glTexGendv:
 * @params: (array zero-terminated=1) (transfer none):
 */
/**
 * glTexGenfv:
 * @params: (array zero-terminated=1) (transfer none):
 */
/**
 * glTexGeniv:
 * @params: (array zero-terminated=1) (transfer none):
 */
/**
 * glTexParameterfv:
 * @params: (array zero-terminated=1) (transfer none):
 */
/**
 * glTexParameteriv:
 * @params: (array zero-terminated=1) (transfer none):
 */
